# nagłówek

paragraf 1

## nagłówek 2

paragraf 2

## nagłówek 3

paragraf 3

~~przekreślenie~~

**pogrubienie**

*kursywa*

>Cytat blokowy

listy:

1. tak
2. taktak
	1. nie
	2. nienie
3. taktaktak
4. taktaktaktak


+ tak
+ tak
	+ nie
	+ nie
+ tak
+ tak

```py
a = 7
b = 6

if a+b=7:
	print("TAK")
elif a+b>7:
	print(":D")
else:
	print("NIE")
```

`print("kod")`
`print("programu")`
`print("a")`

![kot](aa.jpeg)
